// get all data into separate variables before calling sendMQTT - remove functions within functions...
//  timer routine problems with pubsubclient???
//  remove rain sensor interrupts for clarity???
// disable interrupts while sending mqtt???


#include <SPI.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

#include "TimerOne.h"
#include <math.h>

#include <Adafruit_BME280.h> 
#include <BH1750FVI.h>

#define Bucket_Size 0.01 // bucket size to trigger tip count
#define RG11_Pin 3 // digital pin RG11 connected to
#define TX_Pin 8 // used to indicate web data tx
#define DS18B20_Pin 9 // DS18B20 Signal pin on digital 9
#define BME280_ADDRESS (0x76)      // SDO conect to GND address 0x76 or  CSB +3.3V address 0x77  

#define WindSensor_Pin (2) // digital pin for wind speed sensor
#define WindVane_Pin (A3) // analog pin for wind direction sensor
#define VaneOffset 0 // define the offset for caclulating wind direction

volatile unsigned long tipCount; // rain bucket tip counter used in interrupt routine
volatile unsigned long contactTime; // timer to manage any rain contact bounce in interrupt routine

volatile unsigned int timerCount; // used to count ticks for 2.5sec timer count
volatile unsigned long rotations; // cup rotation counter for wind speed calcs
volatile unsigned long contactBounceTime; // timer to avoid contact bounce in wind speed sensor

long lastTipcount; // keep track of bucket tips
float totalRainfall; // total amount of rainfall detected

volatile float windSpeed;
float wspeed;

int vaneValue; // raw analog value from wind vane
int vaneDirection; // translated 0 - 360 wind direction
float calDirection; // calibrated direction after offset applied
int lastDirValue; // last recorded direction value

float lux; // luminosity
float humidity;
float temperature;
float pressure;

// luminosity sensor and bme280
BH1750FVI LightSensor(BH1750FVI::k_DevModeContLowRes);                  // lux sensor address 0x23
Adafruit_BME280 bme ;              // I2C  BME280 sensor

// MQTT stuff
// Function prototypes

// Set your MAC address and IP address here
byte mac[] = { 0xA8, 0x61, 0x0A, 0xAE, 0x5E, 0xED };
IPAddress ip(192, 168, 1, 100);

#define AIO_SERVER      "192.168.1.101"
#define AIO_SERVERPORT  1883
#define AIO_USERNAME    "kazimier_weather"
#define AIO_KEY         "pass"

// Ethernet and MQTT related objects
//Set up the ethernet client
EthernetClient client;
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
// You don't need to change anything below this line!
#define halt(s) { Serial.println(F( s )); while(1);  }

// Setup a feed for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish temp = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/temperature");
Adafruit_MQTT_Publish rain = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/rainfall");
Adafruit_MQTT_Publish hum = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/humidity");
Adafruit_MQTT_Publish pres = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/pressure");
Adafruit_MQTT_Publish wspd = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/wind_speed");
Adafruit_MQTT_Publish wdir = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/wind_direction");
Adafruit_MQTT_Publish lu = Adafruit_MQTT_Publish(&mqtt, "/weathersensor/luminosity");

void setup() {

// setup rain sensor values
lastTipcount = 0;
tipCount = 0;
totalRainfall = 0;

// setup anemometer values
lastDirValue = 0;
rotations = 0;

// setup timer values
timerCount = 0;

// disable the SD card by switching pin 4 High
pinMode(4, OUTPUT);
digitalWrite(4, HIGH);

// start the Ethernet connection and MQTT server
Ethernet.begin(mac, ip);
// Ethernet takes some time to boot!
delay(3000);  
Serial.begin(9600);
Serial.println(Ethernet.localIP());

// MQTT setup
Serial.println(F("Adafruit MQTT demo"));
// Initialise the Client
Serial.print(F("\nInit the Client..."));
delay(1000); //give the ethernet a second to initialize

// start sensors
LightSensor.begin();  
bme.begin(0x76);

pinMode(TX_Pin, OUTPUT);
pinMode(RG11_Pin, INPUT);
pinMode(WindSensor_Pin, INPUT);
attachInterrupt(digitalPinToInterrupt(RG11_Pin), isr_rg, FALLING);
attachInterrupt(digitalPinToInterrupt(WindSensor_Pin), isr_rotation, FALLING);

// setup the timer for 0.5 second
Timer1.initialize(200000);
Timer1.attachInterrupt(isr_timer);

sei();// Enable Interrupts
}


void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

// read data from sensors
readData();

// send MQTT messages
  // Now we can publish stuff!
  Serial.print(F("\nSending data "));
  temp.publish(temperature);
  rain.publish(totalRainfall);
  hum.publish(humidity);
  pres.publish(pressure);
  wspd.publish(windSpeed);  
  wdir.publish(calDirection);
  lu.publish(lux);     

  
  // ping the server to keep the mqtt connection alive
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
delay(1000);
}

void readData () {
  lux = LightSensor.GetLightIntensity();
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure =  bme.readPressure();

  // update rainfall total if required
  if(tipCount != lastTipcount) {
  cli(); // disable interrupts
  lastTipcount = tipCount;
  totalRainfall = tipCount;
  wspeed = windSpeed;
  sei(); // enable interrupts
  }
  //wind
  getWindDirection();
}

// Interrupt handler routine for timer interrupt
void isr_timer() {

timerCount++;

if(timerCount == 5) {
// convert to mp/h using the formula V=P(2.25/T)
// V = P(2.25/2.5) = P * 0.9
windSpeed = rotations * 2.25;
rotations = 0;
timerCount = 0;
}
}

// Interrupt handler routine that is triggered when the rg-11 detects rain
void isr_rg() {

if((millis() - contactTime) > 15 ) { // debounce of sensor signal
tipCount++;
totalRainfall = tipCount;
contactTime = millis();
}
}

// Interrupt handler routine to increment the rotation count for wind speed
void isr_rotation() {

if((millis() - contactBounceTime) > 15 ) { // debounce the switch contact
rotations++;
contactBounceTime = millis();
}
}

// Get Wind Direction
void getWindDirection() {

vaneValue = analogRead(WindVane_Pin);
vaneDirection = map(vaneValue, 0, 1023, 0, 360);
calDirection = vaneDirection + VaneOffset;

if(calDirection > 360)
calDirection = calDirection - 360;

if(calDirection > 360)
calDirection = calDirection - 360;
}

//############################################
// MQTT reconnect routine
//############################################
// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
  }
  Serial.println("MQTT Connected!");
}
